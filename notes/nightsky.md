# Sky observation

Although I don’t get to see it as much and as well as I would like, I care deeply about the starry night sky.
Hopefully some day we’ll have:

 * A GNOME app to [track](http://www.isstracker.com/) [the ISS](WikiPedia:International_Space_Station) and predict when it is a [good time to spot it](http://www.heavens-above.com/PassSummary.aspx?satid=25544&lat=0&lng=0&loc=Unspecified&alt=0&tz=UCT)?
  * Now that I think about it, it could be an app to track any sky/space related event. Anytime there's something worth observing, the app would tell you about it.
  * Maybe integrate with http://eol.jsc.nasa.gov/HDEV/
  * The app would of course be location aware.
  * Tentative names: GNOME SpaceProbe, GNOME SkyObserver, GNOME Telescope.
  * [termtrack](https://github.com/trehn/termtrack) might have some useful things to get inspiration from.
 * A GNOME app version of http://www.sunsurveyor.com/ ?

