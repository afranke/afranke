# Personal notes

Here are a few GNOME related topics I’m interested in.
If any of them piques your interest, feel free to steal ideas or come chat with me about them.

* [GNOME for photographers](photography.md)
* [Sky observation](nightsky.md)
* [Misc](misc.md)
