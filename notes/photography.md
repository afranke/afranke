# GNOME for photographers

We need to improve the experience with RAW photographs.

* Eye of GNOME FIXME: we have Snapshot now, check if this is still relevant
  * can be set as the default program to open them with, but it doesn't know how to Go to the Next or Previous RAW file in the directory
  * doesn't seem to know how to read metadata, fields are empty in the left pane
* Sushi interprets incorrectly rotation metadata and does a flip instead of a rotation. FIXED?
* The properties dialog for JPG files in Nautilus has an "Image" tab with metadata, we should have the same for RAW files.

